<?php

class Transport extends Eloquent {
	public static function select($route=null)
	{
		$return = array();
		$return[''] = 'Selecciona un transporte';
		if($route)
		{
			$transports = DB::table('transports')->where('route_id', '=', (int) $route)->get();

			foreach($transports as $transport)
			{
				$return[$transport->id] = $transport->name;
			}
		}

		return $return;
	}
}