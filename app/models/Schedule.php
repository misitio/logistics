<?php

class Schedule extends Eloquent {
	protected $table = 'schedules';

	public function route()
	{
		return $this->belongsTo('Routes', 'route_id');
	}
}