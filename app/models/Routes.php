<?php

class Routes extends Eloquent {
	protected $table = 'routes';

	protected $guarded = array();

	public static $rules = array();

	public function schedules()
	{
		return $this->hasMany('Schedule');
	}

	public function transports()
	{
		return $this->hasMany('Transport', 'route_id');
	}
}