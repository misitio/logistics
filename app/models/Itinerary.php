<?php

class Itinerary extends Eloquent {
	protected $guarded = array();

	public static $rules = array();

	public static function from_today($schedule_id)
	{
		return DB::select("SELECT * FROM itineraries WHERE DATE(created_at) = CURDATE() AND schedule_id = $schedule_id LIMIT 1");
	}
}