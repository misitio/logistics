<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItinerariesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itineraries', function(Blueprint $table) {
			$table->increments('id');
			
			$table->string('route_id', 11);
			$table->string('schedule_id', 11);
			$table->string('transport_id', 11);
			$table->string('driver_name', 150);
			$table->string('driver_plates', 11)->nullable();
			$table->text('comments');


			$table->softDeletes();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itineraries');
	}

}
