@extends('main')

@section('content')

@if($schedules->count() == 0)
	No hay Horarios.
@else
	<table class="table">
		<thead>
			<tr>
				<th>Ruta</th>
				<th>Hora</th>
				<th>Transporte</th>
				<th>Nombre de chofer</th>
				<th>Placas/N° Unidad</th>
				<th></th>
				<th>Tiempo real</th>
			</tr>
		</thead>
		<tbody>
	@foreach($schedules as $schedule)
		<?php $today = Itinerary::from_today($schedule->id) ?>

		@if(!$today)
			<tr>
				{{ Form::open(array('method' => 'post', 'route' => array('storeItinerary', $schedule->id))) }}
				<td>{{ $schedule->route->name }}</td>
				<td>{{ date("g:i a", strtotime($schedule->time)) }}</td>
				<td>{{ Form::select("transport_id_$schedule->id", Transport::select($schedule->route->id), Input::old("transport_id_$schedule->id"), array('class' => 'form-control	')) }}</td>
				<td>{{ Form::text("driver_name_$schedule->id", Input::old("driver_name_$schedule->id"), array('class' => 'form-control	')) }}</td>
				<td>{{ Form::text("driver_plates_$schedule->id", Input::old("driver_plates_$schedule->id"), array('class' => 'form-control	')) }}</td>
				<td>{{ Form::submit('Enviar', array('class' => 'btn btn-primary')) }}</td>
				{{ Form::close() }}
			</tr>
		@else
			<?php $today = $today[0]; ?>
			 <?php
				$difference = floor((strtotime($today->created_at) - strtotime(date('Y-m-d').' '.$schedule->time) ) / (60));
			  ?>
			<tr class="{{ ($difference >= 15) ? 'danger' : 'success' }}">
				<td>{{ $schedule->route->name }}</td>
				<td>{{ date("g:i a", strtotime($schedule->time)) }}</td>
				<td>{{ Transport::find($today->transport_id)->name }}</td>
				<td>{{ $today->driver_name }}</td>
				<td>{{ $today->driver_plates }}</td>
				<td></td>
				<td>{{ date("g:i a", strtotime($today->created_at)) }}</td>
			</tr>
		@endif
	@endforeach
		</tbody>
	</table>
@endif

@stop