<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ (isset($title)) ? $title : "NO TITLE" }}</title>
	<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
	<style>
	body {
	  padding-top: 50px;
	}
	</style>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Logistics</a>
        <div class="nav-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="#">Diario</a></li>
            <li><a href="#">Reportes</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
	
	<div class="container">
		@yield('content')
	</div>


	<script src="{{ asset('js/jquery.js') }}"></script>
	<script src="{{ asset('js/bootstrap.js') }}"></script>
</body>
</html>