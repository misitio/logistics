<?php

class ItineraryController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($schedule_id)
	{
		$validator = Validator::make(
		    array(
		    	"transport_id_$schedule_id" => Input::get("transport_id_$schedule_id"),
		    	"driver_name_$schedule_id" => Input::get("driver_name_$schedule_id"),
		    ),
		    array(
		    	"driver_name_$schedule_id" => array('required'),
		    	"transport_id_$schedule_id" => array('required')
		    )
		);

		if($validator->fails())
		{
			return Redirect::to('/')->withErrors($validator)->withInput();
		}
		else
		{
			$data = array(
				"schedule_id" => $schedule_id,
				"route_id" => Schedule::find($schedule_id)->route->id,
		    	"transport_id" => Input::get("transport_id_$schedule_id"),
		    	"driver_name" => Input::get("driver_name_$schedule_id"),
		    	"driver_plates" => Input::get("driver_plates_$schedule_id")
		    );

		    $itinerary = new Itinerary($data);
		    $itinerary->save();

		    return Redirect::to('/');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}